﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;

namespace db_terekhov_lab_1
{
    public partial class Form1 : Form
    {
        SqlConnection con;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'lab1DataSet.Workers' table. You can move, or remove it, as needed.
            this.workersTableAdapter.Fill(this.lab1DataSet.Workers);
            // TODO: This line of code loads data into the 'lab1DataSet.Companies' table. You can move, or remove it, as needed.
            this.companiesTableAdapter.Fill(this.lab1DataSet.Companies);

        }

        private void UpdateCompanies(int col, int row) 
        {
            try
            {
                con = new SqlConnection(Properties.Settings.Default.lab1ConnectionString);
                con.Open();

                String rawQuery = "Update Companies SET " + this.lab1DataSet.Companies.Columns[col].ColumnName.ToString() + "=@Value WHERE company_id=@ID";
                SqlCommand cmd = new SqlCommand(rawQuery, con);
                cmd.Parameters.AddWithValue("@Value", this.dataGridView1.Rows[row].Cells[col].Value.ToString());
                cmd.Parameters.AddWithValue("@ID", this.dataGridView1.Rows[row].Cells[0].Value.ToString());
                int rowsAffected = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error\n" + e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            UpdateCompanies(e.ColumnIndex, e.RowIndex);
        }

        private void UpdateWorkers(int col, int row)
        {
            try
            {
                con = new SqlConnection(Properties.Settings.Default.lab1ConnectionString);
                con.Open();

                String rawQuery = "Update Workers SET " + this.lab1DataSet.Workers.Columns[col].ColumnName.ToString() + "=@Value WHERE worker_id=@ID";
                SqlCommand cmd = new SqlCommand(rawQuery, con);
                cmd.Parameters.AddWithValue("@Value", this.dataGridView2.Rows[row].Cells[col].Value.ToString());
                cmd.Parameters.AddWithValue("@ID", this.dataGridView2.Rows[row].Cells[0].Value.ToString());
                int rowsAffected = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error\n" + e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView2_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            UpdateWorkers(e.ColumnIndex, e.RowIndex);
        }
    }
}
